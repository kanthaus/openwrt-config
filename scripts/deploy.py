#!/usr/bin/env python3
import argparse
import yaml
import os
import sys
from string import Template 

parser = argparse.ArgumentParser(description='Deploy config to OpenWRT Access Points')
parser.add_argument(
    'hostname'
)
args = parser.parse_args()

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
configfile = os.path.join(basedir, 'config.yaml')

with open(configfile, 'r') as stream:
    config = yaml.safe_load(stream)


ap = None
for a in config['accesspoints']:
    if a['name'] == args.hostname:
        ap = a
        break

if not ap:
    print("no access point found. maybe hostname is invalid?")
    sys.exit(1)


def execute(cmd):
    c = Template(cmd).substitute(
            basedir=basedir,
            hostname=ap['name'],
            ip=ap['lan_ip']
        )
    os.system(c)

print("generate wireless config...")
execute("python3 $basedir/scripts/OpenWRT-Config-Generator/generate_config.py --config $basedir/config.yaml $hostname $basedir/$hostname/config")

print("upload files...")
execute("rsync -aP --chown=root:root $basedir/common/ root@$ip:/etc/")
execute("rsync -aP --chown=root:root $basedir/$hostname/ root@$ip:/etc/")

print("reload configs...")
execute("ssh root@$ip wifi")
execute("ssh root@$ip /etc/init.d/network reload")
execute("ssh root@$ip /etc/init.d/dnsmasq reload")
execute("ssh root@$ip /etc/init.d/firewall reload")
execute("ssh root@$ip /etc/init.d/odhcpd reload")
execute("ssh root@$ip /etc/init.d/rpcd reload")


print("update package repositories...")
execute("ssh root@$ip opkg update")
print("install packages...")
execute("ssh root@$ip opkg install {}".format(" ".join(ap['packages'])))