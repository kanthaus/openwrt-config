# Build OpenWRT

Follow either the following section or the one after (ath10k-ct preferred) and then the common section later.

## Using ath10k-ct
then, put the patch into 
`package/kernel/ath10k-ct/patches/961-ath10k-ct-always-support-AP-VLAN.patch`

and the `config-ath10k-ct` into the base folder as `.config`

## Using ath10k
then, put the patch into 
`package/kernel/mac80211/patches/ath/982-ath10k-fix-dynamic-vlan.patch`

and the `config-ath10k` into the base folder as `.config`

## Common

Run `make` and wait a few hours (first time or after tree update) or just a few minutes (after you did some config changes).

The build will generate an image for TPLink Archer C5, using a dynamic vlan enabled ath10k driver. Also, it adds luci-ssl, sqm and rsync.
